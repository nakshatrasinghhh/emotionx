import cv2
from model import FacialExpressionModel
import numpy as np

facec = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
model = FacialExpressionModel("model.json", "model_weights.h5")
font = cv2.FONT_HERSHEY_SIMPLEX

class VideoCamera(object):
    def __init__(self):
        # use default id as 0 to access camera
        self.video = cv2.VideoCapture(0)

    def __del__(self):
        # close the window
        self.video.release()

    # returns camera frames along with bounding boxes and predictions
    def get_frame(self):
        # read returns 2 things, one is whether the frame was captured successfully and the other is the image
        _, fr = self.video.read()
        # convert to gray scale
        gray_fr = cv2.cvtColor(fr, cv2.COLOR_BGR2GRAY)
        # detects faces of different sizes in the input image 
        faces = facec.detectMultiScale(gray_fr, 1.3, 10)        #close or far from the camera

        # faces returns co-ordinates, width and height
        for (x, y, w, h) in faces:
            # array slicing y:y+h means roi height and x:x+w is roi width 
            fc = gray_fr[y:y+h, x:x+w] 
            # reshape to size to which the model was trained
            roi = cv2.resize(fc, (48, 48))
            # predict the emotion
            pred = model.predict_emotion(roi[np.newaxis, :, :, np.newaxis])  #numpy.newaxis is used to increase the dimension of the existing array by one more dimension, when used once
            # print the emotion prediction
            cv2.putText(fr, pred, (x, y), font, 1, (0, 0, 255), 3)
            # draw the rectangle
            cv2.rectangle(fr,(x,y),(x+w,y+h), (255, 0, 0), 3)

        _, jpeg = cv2.imencode('.jpg', fr)
        return jpeg.tobytes()  